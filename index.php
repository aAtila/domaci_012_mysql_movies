<?php
    define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
    require('inc/db_config.php');

    include("inc/header.html");
    include("inc/menu.html");

    if(!isset($_GET["link"]))
    $_GET["link"]="index";

    switch ($_GET["link"])
    {
        case "movies":
        include("inc/movies.php");
        break;
        
        case "genre":
        include("inc/genre.php");
        break;
        
        case "schedule":
        include("inc/schedule.php");
        break;

        case "insert_movie":
        include("inc/insert-movie-form.php");
        break;
        
        default:
        include("inc/movies.php");
        break;
    }
    include("inc/footer.html");
?>