<?php

    //Kreirati web stranicu update.php koja na osnovu odabira podatka iz tabele film prikazuje podatke odabranog filma u obrascu

    define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
    require('inc/db_config.php');

    if(isset($_GET["movieID"]))
    $movieID = $_GET["movieID"];

    $sql = "SELECT * FROM movies WHERE id = $movieID";
    $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

    if(mysqli_num_rows($result)>0)
    {

        //$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
        //var_dump($row);
        
        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
        {
            $rowID = $row["id"];
            $rowName = $row["name"];
            $rowIDGenre = $row["id_genre"];
            $rowTime = $row["time"];
            $rowSummary = $row["summary"];

        }

        mysqli_free_result($result);
    }

    mysqli_close($connection);

    
    include("inc/header.html");
    include("inc/menu.html");
?>

<h2>Update Movie - ID <?php echo $rowID; ?></h2>

<form action="realUpdate.php" method="POST">
    <lable>Name</lable><br>
    <input type="text" name="name" value="<?php echo $rowName; ?>"><br><br>
    <lable>Genre ID</lable><br>
    <input type="text" name="id_genre" value="<?php echo $rowIDGenre; ?>"><br><br>
    <lable>Time</lable><br>
    <input type="text" name="time" value="<?php echo $rowTime; ?>"><br><br>
    <lable>Summary</lable><br>
    <textarea name="summary" rows="10" cols="50"><?php echo $rowSummary; ?></textarea><br><br>
    <input type="hidden" name="id_movie" value="<?php echo $rowID; ?>">
    <input type="submit" value="Update">
</form>

<?php include("inc/footer.html"); ?>