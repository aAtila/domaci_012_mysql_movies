<?php

$sql = "SELECT * FROM genre";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{
 
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        //echo $row["id"]." ".$row["name"]." ".$row["id_genre"]." ".$row["time"]." ".$row["summary"]. "<br><hr>";
        $genreID = $row["id"];
        $genreName = $row["name"];
        echo "<option value="$genreID">$genreName</option>";

    }
    echo "</table><br>";

    if ($result = mysqli_query($connection, "SELECT * FROM movies")) {
        printf("Movies returned %d rows.<br><br>\n", mysqli_num_rows($result));
    }

    mysqli_free_result($result);
}

mysqli_close($connection);

?>

<h2>Add Movie</h2>

<form action="insert.php" method="POST">
    <label>Name</label><br>
    <input type="text" name="name"><br><br>
    <label>Genre</label><br>
    <select name="id_genre">
    <option value="1">Crime</option>
    <option value="2">Drama</option>
    <option value="3">History</option>
    <option value="4">Horror</option>
    <option value="5">Romance</option>
    <option value="6">Thriller</option>
    </select><br><br>
    <label>Time (in minutes)</label><br>
    <input type="text" name="time"><br><br>
    <label>Summary</label><br>
    <textarea name="summary" rows="10" cols="50"></textarea><br><br>
    <input type="submit" value="Add">
</form>