<?php

echo '<h2>Movies</h2>';

$sql = "SELECT movies.id as 'ID', movies.name as 'Title', genre.name as 'Genre', schedule.time as 'Time', movies.summary as 'Summary' FROM movies
        INNER JOIN genre ON movies.id_genre = genre.id
        INNER JOIN schedule ON movies.id = schedule.id_movie";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{

    //$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
    //var_dump($row);
    echo "<table class=\"w3-table-all\">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Genre</th>
                <th>Time</th>
                <th>Summary</th>
            </tr>";
    
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        //echo $row["id"]." ".$row["name"]." ".$row["id_genre"]." ".$row["time"]." ".$row["summary"]. "<br><hr>";
        $rowID = $row["ID"];
        echo "<tr>
                <td><a href=\"update.php?movieID=$rowID\">$rowID</a></td>
                <td>".$row["Title"]."</td>
                <td>".$row["Genre"]."</td>
                <td>".$row["Time"]."</td>
                <td>".$row["Summary"]."</td>
                <td><a href=\"delete.php?movieID=$rowID\">delete</a></td>
              </tr>";

    }
    echo "</table><br>";

    if ($result = mysqli_query($connection, "SELECT * FROM movies")) {
        printf("Movies returned %d rows.<br><br>\n", mysqli_num_rows($result));
    }

    mysqli_free_result($result);
}

mysqli_close($connection);