SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `local`;
CREATE DATABASE `local` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `local`;

DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `genre` (`id`, `name`) VALUES
(1,	'crime'),
(2,	'drama'),
(3,	'history'),
(4,	'horror'),
(5,	'romance'),
(6,	'thriller');

DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `summary` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_genre` (`id_genre`),
  CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `movies` (`id`, `name`, `id_genre`, `time`, `summary`) VALUES
(1,	'The Godfather',	1,	175,	'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.'),
(2,	'The Shawshank Redemption',	2,	142,	'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.'),
(3,	'Schindler\'s List',	3,	195,	'In German-occupied Poland during World War II, Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazi Germans.'),
(4,	'Psycho',	4,	109,	'A Phoenix secretary embezzles $40,000 from her employer\'s client, goes on the run, and checks into a remote motel run by a young man under the domination of his mother.'),
(5,	'Casablanca',	5,	102,	'A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.'),
(6,	'Gone with the Wind',	5,	238,	'A manipulative woman and a roguish man conduct a turbulent romance during the American Civil War and Reconstruction periods.'),
(7,	'One Flew Over the Cuckoo\'s Nest',	2,	133,	'A criminal pleads insanity after getting into trouble again and once in the mental institution rebels against the oppressive nurse and rallies up the scared patients.'),
(8,	'Forrest Gump',	2,	142,	'The presidencies of Kennedy and Johnson, Vietnam, Watergate, and other history unfold through the perspective of an Alabama man with an IQ of 75.'),
(10,	' The Silence of the Lambs',	6,	118,	'A young F.B.I. cadet must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer, a madman who skins his victims.'),
(11,	'Some Like It Hot',	5,	121,	'When two male musicians witness a mob hit, they flee the state in an all-female band disguised as women, but further complications set in.');

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` date NOT NULL,
  `id_movie` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movie` (`id_movie`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`id_movie`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `schedule` (`id`, `time`, `id_movie`) VALUES
(1,	'1999-12-29',	1),
(2,	'2000-11-22',	2),
(3,	'2000-12-04',	3),
(4,	'2001-06-06',	4),
(5,	'2002-01-21',	5),
(6,	'2004-03-19',	6),
(7,	'2004-12-13',	7),
(8,	'2005-10-10',	8),
(10,	'2006-02-23',	10),
(11,	'2006-09-27',	11);

-- 2018-04-04 22:30:29